package com.hitex.jobEVN.repository;


import com.hitex.jobEVN.model.Order;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface OrderRepository extends CrudRepository<Order, String> {
    @Query(value = "SELECT * FROM sale.order WHERE status = 102 or status = 103" ,nativeQuery = true)
    List<Order> findOrderAuthenOTP();

    @Query(value = "SELECT * FROM sale.order WHERE status = 201 OR status = 202 " +
            "AND payment_type = 3" ,nativeQuery = true)
    List<Order> findOrderTransfer();

    @Modifying(clearAutomatically = true,flushAutomatically = true)
    @Transactional
    @Query("UPDATE Order ode SET ode.status=:status  where ode.id=:orderId")
    void updateOrder(@Param("status") int status, @Param("orderId") Integer orderId);

}
