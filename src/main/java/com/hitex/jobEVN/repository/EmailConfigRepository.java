package com.hitex.jobEVN.repository;

import com.hitex.jobEVN.model.EmailConfig;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface EmailConfigRepository extends CrudRepository<EmailConfig,String> {
    @Query("SELECT cfg FROM EmailConfig cfg WHERE cfg.action = :action")
    EmailConfig getContentEmail(@Param("action")String action);
}
