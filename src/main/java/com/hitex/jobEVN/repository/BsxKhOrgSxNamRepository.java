package com.hitex.jobEVN.repository;

import com.hitex.jobEVN.model.BsxKhOrgSxNam;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface BsxKhOrgSxNamRepository extends CrudRepository<BsxKhOrgSxNam, String> {
    @Query("SELECT b FROM BsxKhOrgSxNam b WHERE NOT EXISTS " +
            "(select 1 from BsxKhOrgSxNam old WHERE b.id = old.id)")
    BsxKhOrgSxNam getBsxKhOrgSxNam();
}
