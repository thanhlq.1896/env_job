package com.hitex.jobEVN.repository;

import com.hitex.jobEVN.model.LogFreeingResourceBefore;
import org.springframework.data.repository.CrudRepository;

public interface LogFreeingRessourceBeforeRepository extends CrudRepository<LogFreeingResourceBefore,String> {
}
