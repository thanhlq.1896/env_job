package com.hitex.jobEVN.repository;

import com.hitex.jobEVN.model.RetailOrder;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface RetailOrderRepository extends CrudRepository<RetailOrder,String> {
    @Query("Select retail from RetailOrder retail where retail.orderId =:orderId and retail.isdnType = 2 ")
    List<RetailOrder> findRetailByOrderId(@Param("orderId") int orderId);

    @Modifying(clearAutomatically = true,flushAutomatically = true)
    @Transactional
    @Query("UPDATE RetailOrder retail SET retail.status =:status  where retail.id =:retailId")
    void updateRetail(@Param("status") int status, @Param("retailId") int retailId);

    @Query("Select retail from RetailOrder retail where retail.isdn =:isdn and retail.status <> 1 AND retail.status <> 7")
    List<RetailOrder> findIsdnRetail(@Param("isdn") String isdn);

    @Query("Select retail from RetailOrder retail where retail.simSerial =:imSerial and retail.status <> 1 AND retail.status <> 7")
    List<RetailOrder> findimSerialRetail(@Param("imSerial") String imSerial);
}
