package com.hitex.jobEVN.repository;

import com.hitex.jobEVN.model.EmailHistory;
import org.springframework.data.repository.CrudRepository;

public interface EmailHistoryRepository extends CrudRepository<EmailHistory,String> {

}
