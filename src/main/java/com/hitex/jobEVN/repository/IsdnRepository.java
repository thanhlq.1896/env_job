package com.hitex.jobEVN.repository;

import com.hitex.jobEVN.model.Isdn;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

public interface IsdnRepository extends CrudRepository<Isdn,String> {
    @Query("SELECT isdn from Isdn isdn where isdn.isdn=:isdn")
    Isdn findIsdn(@Param("isdn") String isdn);

    @Modifying(clearAutomatically = true,flushAutomatically = true)
    @Transactional
    @Query("UPDATE Isdn isdn SET isdn.sold =:sold  where isdn.isdn =:isdn")
    void updateIsdn(@Param("sold") int sold, @Param("isdn") String isdn);
}
