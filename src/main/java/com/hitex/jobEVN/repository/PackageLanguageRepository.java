package com.hitex.jobEVN.repository;

import com.hitex.jobEVN.model.PackageLanguage;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface PackageLanguageRepository extends CrudRepository<PackageLanguage,String> {
    @Query("select pack from PackageLanguage pack where pack.packageId=:packId and pack.language=:lang")
    PackageLanguage getPackInfo(@Param("packId") int packId, @Param("lang") String lang);
}
