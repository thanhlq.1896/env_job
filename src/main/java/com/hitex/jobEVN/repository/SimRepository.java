package com.hitex.jobEVN.repository;

import com.hitex.jobEVN.model.Sim;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

public interface SimRepository extends CrudRepository<Sim,String> {

    @Query("SELECT sim from Sim sim where sim.imSerial=:imSerial")
    Sim findSim(@Param("imSerial") String imSerial);

    @Modifying(clearAutomatically = true,flushAutomatically = true)
    @Transactional
    @Query("UPDATE Sim sim SET sim.sold =:sold  where sim.imSerial =:imSerial")
    void updateSim(@Param("sold") int sold, @Param("imSerial") String imSerial);
}
