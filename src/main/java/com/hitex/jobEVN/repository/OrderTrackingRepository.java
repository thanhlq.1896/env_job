package com.hitex.jobEVN.repository;

import com.hitex.jobEVN.model.OrderTracking;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface OrderTrackingRepository extends CrudRepository<OrderTracking, String> {
    @Query(value = "SELECT * from sale.order_tracking  orderTrack where orderTrack.order_id=:orderId order by  orderTrack.time desc limit 1",nativeQuery = true)
    OrderTracking  getTime(@Param("orderId") int orderId);
}
