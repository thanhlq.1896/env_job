package com.hitex.jobEVN.repository;

import com.hitex.jobEVN.model.BsxVhPlantDay;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface BsxVhPlantDayRepository extends CrudRepository<BsxVhPlantDay, String> {
    @Query("SELECT b FROM BsxVhPlantDay b WHERE NOT EXISTS " +
            "(select 1 from BsxVhPlantDay old WHERE b.id = old.id)")
    BsxVhPlantDay getBsxVhPlantDay();
}
