package com.hitex.jobEVN.repository;

import com.hitex.jobEVN.model.BsxKhPlantSxNam;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface BsxKhPlantSxNamRepository extends CrudRepository<BsxKhPlantSxNam,String> {
    @Query("SELECT b FROM BsxKhPlantSxNam b WHERE NOT EXISTS " +
            "(select 1 from BsxKhPlantSxNam old WHERE b.id = old.id)")
    BsxKhPlantSxNam getBsxKhPlantSxNam();
}
