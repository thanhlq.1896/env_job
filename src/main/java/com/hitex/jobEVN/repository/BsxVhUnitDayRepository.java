package com.hitex.jobEVN.repository;

import com.hitex.jobEVN.model.BsxVhUnitDay;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface BsxVhUnitDayRepository extends CrudRepository<BsxVhUnitDay, String> {
    @Query("SELECT b FROM BsxVhUnitDay b WHERE NOT EXISTS " +
            "(select 1 from BsxVhUnitDay old WHERE b.id = old.id)")
    BsxVhUnitDay getBsxVhUnitDay();
}
