package com.hitex.jobEVN.repository;

import com.hitex.jobEVN.model.OrderConfig;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface OrderConfigRepository extends CrudRepository<OrderConfig,String> {
        @Query(value = "select oc from OrderConfig oc where oc.code LIKE %:code% and oc.status = 1")
        List<OrderConfig> getCode(@Param("code") String code);
}
