package com.hitex.jobEVN.datasource;

import com.jolbox.bonecp.BoneCPDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;


@Configuration
@ComponentScan
@PropertySource("classpath:application.properties")
public class DataSourceConfig {

    @Value("${spring.datasource.driver-class-name}")
    private String driverName;

    @Value("${spring.datasource.url}")
    private String url;

    @Value("${spring.datasource.username}")
    private String username;

    @Value("${spring.datasource.password}")
    private String password;

    @Value("${spring.datasource.hikari.maximumPoolSize}")
    private int maxConnection;

    @Bean("read_data_service")
    public BoneCPDataSource boneCPDataSource() {
        BoneCPDataSource boneCPDataSource = new BoneCPDataSource();
        boneCPDataSource.setDriverClass(this.driverName);
        boneCPDataSource.setJdbcUrl(this.url);
        boneCPDataSource.setUsername(this.username);
        boneCPDataSource.setPassword(this.password);
        boneCPDataSource.setMaxConnectionsPerPartition(maxConnection);
        return boneCPDataSource;

    }

    @Bean("entityManagerFactory")
    public EntityManagerFactory entityManagerFactory(@Qualifier("read_data_service") BoneCPDataSource BoneCPDataSource) {

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(false);
        vendorAdapter.setShowSql(false);

        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setPackagesToScan("com.hitex.electricityProduction");
        factory.setDataSource(BoneCPDataSource);
        factory.afterPropertiesSet();
        return factory.getObject();
    }

    @Bean("transactionManager")
    public PlatformTransactionManager transactionManager(@Qualifier("entityManagerFactory") EntityManagerFactory EntityManagerFactory) {
        JpaTransactionManager txManager = new JpaTransactionManager();
        txManager.setEntityManagerFactory(EntityManagerFactory);
        return txManager;
    }
}
