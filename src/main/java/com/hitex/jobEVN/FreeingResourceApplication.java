package com.hitex.jobEVN;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class FreeingResourceApplication {

    public static void main(String[] args) {
        SpringApplication.run(FreeingResourceApplication.class, args);
    }

}
