package com.hitex.jobEVN.utils;

import lombok.extern.log4j.Log4j2;

import java.util.Base64;

@Log4j2
public class EncryptsUtil {
    public static String decodeBase64(String encoded) {
        return new String(Base64.getDecoder().decode(encoded));
    }
}
