package com.hitex.jobEVN.utils;

import java.time.format.DateTimeFormatter;

public class Constant {
    public static final String ORDER = "order";
    public static final String EMAIL_CONFIG = "email_config";
    public static final String EMAIL_HISTORY = "email_history";
    public static final String ORDER_TRACKING = "order_tracking";
    public static final String ORDER_STATUS = "order_status";
    public static final String ORDER_CONFIG = "order_config";
    public static final String LOG_FREEING_RESOURCE_BEFORE = "log_freeing_resource_before";
    public static final String LOG_FREEING_RESOURCE_AFTER = "log_freeing_resource_after";
    public static final String ISDN = "isdn";
    public static final String SIM = "sim";
    public static final String SALE = "sale";
    public static final String SERVICE_CATALOG = "service_catalog";
    public static final String GENERAL = "general";
    public static final String INVENTORY = "inventory";
    public static final String RETAIL_ORDER = "retail_order";
    public static final int ORDER_CANCEL = 5;
    public static final int WAIT_CANCEL = 202;
    public static final int RO_REJECTED = 7;
    public static final int NOT_SOLD = 0;
    public static final int SOLD = 1;
    public static final int WAIT_CONFIRM_OTP_MAIL = 102;
    public static final int CONFIRMED_OTP_MAIL = 103;
    public static final int WAIT_TRANSFER = 201;
    public static final String TIME_WAIT_BANK_TRANSFER = "TIME_WAIT_BANK_TRANSFER";
    public static final String TIME_WAIT_CONFIRM_INFO = "TIME_WAIT_CONFIRM_INFO";
    public static final String TIME_WAIT_CANCEL_BANK_TRANSFER = "TIME_WAIT_CANCEL_BANK_TRANSFER";
    public static final String TIME_WAIT_BANK_TRANSFER_MINUTE = "TIME_WAIT_BANK_TRANSFER_MINUTE";
    public static final String TIME_WAIT_BANK_TRANSFER_DATE = "TIME_WAIT_BANK_TRANSFER_DATE";
    public static final String TIME_WAIT_CANCEL_BANK_TRANSFER_MINUTE = "TIME_WAIT_CANCEL_BANK_TRANSFER_MINUTE";
    public static final String TIME_WAIT_CANCEL_BANK_TRANSFER_DATE = "TIME_WAIT_CANCEL_BANK_TRANSFER_DATE";
    public static final String TIME_WAIT_CONFIRM_INFO_MINUTE = "TIME_WAIT_CONFIRM_INFO_MINUTE";
    public static final String TIME_WAIT_CONFIRM_INFO_DATE = "TIME_WAIT_CONFIRM_INFO_DATE";
    public static final DateTimeFormatter formatDate = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static final DateTimeFormatter formatDateV2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public static final String CANCEL_ORDER = "CANCEL_ORDER";
    public static final String LIST_ORDER = "<tr>\n" +
            "                    <td style=\"width: 190px; padding: 15px 0; border-bottom: 1px solid #333;\">\n" +
            "                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" style=\"text-align: center;\">\n" +
            "                            <tr>\n" +
            "                                <td>Mã đơn hàng</td>\n" +
            "                            </tr>\n" +
            "                            <tr>\n" +
            "                                <td>\n" +
            "                                    <img style=\"max-width: 100%;\" src=\"__qr_code__\" width=\"120px\" height=\"120px\" />\n" +
            "                                </td>\n" +
            "                            </tr>\n" +
            "                            <tr>\n" +
            "                                <td style=\"font-weight: bold;\">__retail_code__</td>\n" +
            "                            </tr>\n" +
            "                        </table>\n" +
            "                    </td>\n" +
            "                    <td style=\"padding: 15px 0; border-bottom: 1px solid #333;\">\n" +
            "                        <table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; background-color: #e3e3e3; border-radius: 3px; padding: 15px;\">\n" +
            "                            <tr>\n" +
            "                                <td>\n" +
            "                                    <table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; padding-right: 5px;\">\n" +
            "                                        <tr>\n" +
            "                                            <td style=\"width: 50%; padding-bottom: 5px;\">Số điện thoại:</td>\n" +
            "                                            <td style=\"padding-bottom: 5px; font-weight: bold;\">__isdn__</td>\n" +
            "                                        </tr>\n" +
            "                                        <tr>\n" +
            "                                            <td style=\"width: 50%; padding-bottom: 5px;\">Serial SIM:</td>\n" +
            "                                            <td style=\"padding-bottom: 5px; font-weight: bold;\">__serial__</td>\n" +
            "                                        </tr>\n" +
            "                                        <tr>\n" +
            "                                            <td style=\"width: 50%; padding-bottom: 5px;\">Giá:</td>\n" +
            "                                            <td style=\"padding-bottom: 5px; font-weight: bold;\">__isdn_price__ VNĐ</td>\n" +
            "                                        </tr>\n" +
            "                                    </table>\n" +
            "                                </td>\n" +
            "                               _PACKAGE_\n" +
            "                            </tr>\n" +
            "                        </table>\n" +
            "                    </td>\n" +
            "                </tr>";
    public final static String PACK = " <td>\n" +
            "                                    <table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 100%; padding-left: 5px;\">\n" +
            "                                        <tr>\n" +
            "                                            <td style=\"width: 50%; padding-bottom: 5px;\">Gói cước:</td>\n" +
            "                                            <td style=\"padding-bottom: 5px; font-weight: bold;\">__package_name__</td>\n" +
            "                                        </tr>\n" +
            "                                        <tr>\n" +
            "                                            <td style=\"width: 50%; padding-bottom: 5px;\">Giá:</td>\n" +
            "                                            <td style=\"padding-bottom: 5px; font-weight: bold;\">__package_price__ VNĐ</td>\n" +
            "                                        </tr>\n" +
            "                                        <tr>\n" +
            "                                            <td style=\"width: 50%; padding-bottom: 5px;\">Ngày hết hạn:</td>\n" +
            "                                            <td style=\"padding-bottom: 5px; font-weight: bold;\">__expire_date__</td>\n" +
            "                                        </tr>\n" +
            "                                    </table>\n" +
            "                                </td>";

    public final static int DELIVERY_TYPE_SHIP_NORMAL = 1;
//    public final static int DELIVERY_TYPE_SHIP_FAST = 2;
    public final static String DELIVERY_TYPE_NORMAL = "Ship thường";
    public final static String DELIVERY_TYPE__FAST = "Ship nhanh";

}
