package com.hitex.jobEVN.utils;

import com.hitex.jobEVN.model.EmailConfig;
import com.sun.mail.smtp.SMTPTransport;
import lombok.extern.log4j.Log4j2;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Properties;

@Log4j2
public class EmailUtil {

    public static boolean send(EmailConfig config, String recipient, String content) {
        Properties properties = new Properties();
        properties.put("mail.smtps.host", config.getHost());
        properties.put("mail.smtps.auth", config.getAuth() == 1);

        Session session = Session.getInstance(properties);
        MimeMessage message = new MimeMessage(session);
        try {
            //email người gửi + tên người gửi ( tên hiển thị bên ngoài ) -- Mạnh Tùng <manhtung.hmt@gmail.com>
            message.setFrom(new InternetAddress(config.getEmail(), config.getName()));
            //người nhận
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient));
            //chủ đề của mail
            message.setSubject(config.getSubject());
            //cấu trúc mail
            BodyPart bodyPart = new MimeBodyPart();
            bodyPart.setContent(content, "text/html; charset=UTF-8");

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(bodyPart);
            //thiết lập nội dụng mail
            message.setContent(multipart);
            //gủi mail
            SMTPTransport transport = (SMTPTransport) session.getTransport("smtps");
            transport.connect(config.getHost(), config.getEmail(), EncryptsUtil.decodeBase64(config.getPassword()));
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();

            return true;
        } catch (Exception e) {
            log.error(e);
        }

        return false;
    }
}
