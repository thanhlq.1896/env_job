package com.hitex.jobEVN.model;

import com.hitex.jobEVN.utils.Constant;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name =  Constant.LOG_FREEING_RESOURCE_BEFORE,schema = Constant.SALE)
@Entity
@Data
public class LogFreeingResourceBefore {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "retail_id")
    private Integer retailId;
    @Column(name = "order_id")
    private Integer orderId;
    @Column(name = "status_retail")
    private Integer statusRetail;
    @Column(name = "status_order")
    private Integer statusOrder;
    @Column(name = "isdn")
    private String isdn;
    @Column(name = "im_serial")
    private String imSerial;
    @Column(name = "sold")
    private Integer sold;
    @Column(name = "process_date")
    private LocalDateTime processDate;
    @Column(name = "status_retail_after")
    private Integer statusRetailAfter;
    @Column(name = "status_order_after")
    private Integer statusOrderAfter;
    @Column(name = "sold_after")
    private Integer soldAfter;
    @Column(name = "payment_type_before")
    private Integer paymentTypeBefore;
    @Column(name = "payment_type_after")
    private Integer paymentTypeAfter;
    private String description;


}
