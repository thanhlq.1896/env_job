package com.hitex.jobEVN.model;

import com.hitex.jobEVN.utils.Constant;
import lombok.Data;

import javax.persistence.*;

@Table(name =  Constant.RETAIL_ORDER,schema = Constant.SALE)
@Entity
@Data
public class RetailOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "order_id")
    private Integer orderId;
    @Column(name = "status")
    private Integer status;
    @Column(name = "isdn")
    private String isdn;
    @Column(name = "sim_serial")
    private String simSerial;
    @Column(name = "isdn_type")
    private Integer isdnType;
    @Column(name = "package_id")
    private Integer packageId;
    @Column(name = "package_price")
    private Integer packagePrice;
    @Column(name = "lang")
    private String language;
    @Column(name = "isdn_price")
    private Integer isdnPrice;
    @Column(name = "code")
    private String code;
    @Column(name = "qr_code")
    private String qrCode;
}
