package com.hitex.jobEVN.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "bsx_vh_plant_day")
public class BsxVhPlantDay {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "plantid")
    private String plantId;
    @Column(name = "day")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime day;
    @Column(name = "UTK")
    private double utk;
    @Column(name = "EMF")
    private double emf;
    @Column(name = "EMF_K")
    private double emfK;
    @Column(name = "EMF_DO")
    private double emfDo;
    @Column(name = "EMF_FO")
    private double emfFo;
    @Column(name = "EMF_DH")
    private double emfDh;
    @Column(name = "ETD")
    private double etd;
    @Column(name = "ETD_K")
    private double etdK;
    @Column(name = "ETD_DO")
    private double etdDo;
    @Column(name = "ETD_FO")
    private double etdFo;
    @Column(name = "ETD_DH")
    private double etdDh;
    @Column(name = "ETDSXD")
    private double etdSxd;
    @Column(name = "ETDXS")
    private double etdXs;
    @Column(name = "ETDCB")
    private double etdCb;
    @Column(name = "EDB")
    private double edb;
    @Column(name = "EDB_K")
    private double edbK;
    @Column(name = "EDB_DO")
    private double edbDo;
    @Column(name = "EDB_FO")
    private double edbFo;
    @Column(name = "EDB_DH")
    private double edbDh;
    @Column(name = "NLthantttc_SX")
    private double nLThanTTTCSx;
    @Column(name = "NLthantttc_KD")
    private double nLThanTTTCKd;
    @Column(name = "NLthantttc_SCL")
    private double nLThanTTTCScl;
    @Column(name = "NLthantttc_T")
    private double nLThanTTTCT;
    @Column(name = "NLthantttn_SX")
    private double nLThanTTTNSx;
    @Column(name = "NLthantttn_KD")
    private double nLThanTTTNKd;
    @Column(name = "NLthantttn_SCL")
    private double nLThanTTTNScl;
    @Column(name = "NLthantttn_T")
    private double nLThanTTTNT;
    @Column(name = "NLThantontc_T")
    private double nLThanTonTCT;
    @Column(name = "NLthantontn_T")
    private double nLThanTonTNT;
    @Column(name = "NLdaudott_SX")
    private double nLDauDOTTSx;
    @Column(name = "NLdaudott_KD")
    private double nLDauDOTTKd;
    @Column(name = "NLdaudott_SCL")
    private double nLDauDOTTScl;
    @Column(name = "NLdaudott_T")
    private double nLDauDOTTT;
    @Column(name = "NLdaudoton_T")
    private double nLDauDOTon_T;
    @Column(name = "NLdaufott_SX")
    private double nLDauFOTTSx;
    @Column(name = "NLdaufott_KD")
    private double nLDauFOTTKd;
    @Column(name = "NLdaufott_SCL")
    private double nLDauFOTTScl;
    @Column(name = "NLdaufott_T")
    private double nLDauFOTTT;
    @Column(name = "NLdaufoton_T")
    private double nLDauFOTonT;
    @Column(name = "NLkhitt_SX")
    private double nLKhiTTSx;
    @Column(name = "NLkhitt_KD")
    private double nLKhiTTKd;
    @Column(name = "NLkhitt_SCL")
    private double nLKhiTTScl;
    @Column(name = "NLkhitt_T")
    private double nLKhiTTT;
    @Column(name = "USER_ID_DIEN")
    private String userIdDien;
    @Column(name = "USER_DTIME_DIEN")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime userDtimeDien;
    @Column(name = "USER_ID_NL")
    private String userIdNl;
    @Column(name = "USER_DTIME_NL")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime userDtimeNl;
    @Column(name = "DIEN_NANG_NHAN")
    private double dienNangNhan;
    @Column(name = "DIEN_NANG_NHAN_CHAY_BU")
    private double dienNangNhanChayBu;
    @Column(name = "DIEN_NANG_PHAN_KHANG")
    private double dienNangPhanKhang;
    @Column(name = "DO_PHAT_THAI")
    private double doPhatThai;
    @Column(name = "TON_THAT_MBA_KICH_TU")
    private double tonThatMbaKichTu;
    @Column(name = "TON_THAT_MBA_NANG")
    private double tonThatMbaNang;
    @Column(name = "DAT_KHI_PHAT_THAI")
    private double datKhiPhatThai;
    @Column(name = "AUTO_CAL_FORMULA")
    private double autoCalFormula;
    @Column(name = "NLdavoiTT")
    private double nLDaVoiTT;
    @Column(name = "NLdavoiton")
    private double nLDaVoiTon;
    @Column(name = "NLdavoiTT_KD")
    private double nLDaVoiTTKd;
    @Column(name = "NLdavoiTT_SCL")
    private double nLDaVoiTTScl;
    @Column(name = "NLdavoiTT_SX")
    private double nLDaVoiTTSx;
    @Column(name = "NLnuocTT")
    private double nLNuocTT;
    @Column(name = "EDB_DH_DAU")
    private double edbDhDau;
    @Column(name = "DN_PHANKHANG_NHAN")
    private double dnPhankhangNhan;
    @Column(name = "DN_MBA_KICH_TU_TE")
    private double dnMbaKichTuTe;
    @Column(name = "DN_TD_TD9")
    private double dnTdTd9;
    @Column(name = "Nldaudo_NHIETTRI")
    private double nlDauDoNhietTri;
    @Column(name = "Nldaufo_NHIETTRI")
    private double nlDauFoNhietTri;


}
