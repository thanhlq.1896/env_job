package com.hitex.jobEVN.model;

import com.hitex.jobEVN.utils.Constant;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name =  Constant.EMAIL_HISTORY,schema = Constant.GENERAL)
@Entity
@Data
public class EmailHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String recipient;

    private String sender;
    @Column(length = 500)
    private String subject;
    @Column(columnDefinition = "text")
    private String content;
    private String action;

    @Column(name = "create_date")
    private LocalDateTime createDate;
}
