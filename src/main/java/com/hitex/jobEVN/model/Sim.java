package com.hitex.jobEVN.model;

import com.hitex.jobEVN.utils.Constant;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name =  Constant.SIM,schema = Constant.INVENTORY)
@Entity
@Data
public class Sim {
    @Id
    @Column(name = "im_serial")
    private String imSerial;
    @Column(name = "sold")
    private int sold;
}
