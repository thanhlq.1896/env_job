package com.hitex.jobEVN.model;

import com.hitex.jobEVN.utils.Constant;
import lombok.Data;

import javax.persistence.*;


@Entity
@Table(name = "package_language",schema = Constant.SERVICE_CATALOG)
@Data
public class PackageLanguage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "language")
    private String language;
    @Column(name = "package_id")
    private Integer packageId;
    @Column(name = "title")
    private String title;
}
