package com.hitex.jobEVN.model;

import com.hitex.jobEVN.utils.Constant;
import lombok.Data;

import javax.persistence.*;

@Table(name =  Constant.EMAIL_CONFIG,schema = Constant.GENERAL)
@Entity
@Data
public class EmailConfig {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "action")
    private String action;
    @Column(name = "auth")
    private Integer auth;
    @Column(name = "email")
    private String email;
    @Column(name = "host")
    private String host;
    @Column(name = "name")
    private String name;
    @Column(name = "password")
    private String password;
    @Column(name = "content")
    private String content;
    @Column(name = "subject")
    private String subject;
}
