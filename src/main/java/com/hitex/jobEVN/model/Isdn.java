package com.hitex.jobEVN.model;

import com.hitex.jobEVN.utils.Constant;
import lombok.Data;

import javax.persistence.*;

@Table(name =  Constant.ISDN,schema = Constant.INVENTORY)
@Entity
@Data
public class Isdn {
    @Id
    @Column(name = "isdn")
    private String isdn;
    @Column(name = "sold")
    private int sold;
}
