package com.hitex.jobEVN.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "bsx_kh_org_sx_nam")
public class BsxKhOrgSxNam {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "orgid")
    private String orgId;
    @Column(name = "nam")
    private int nam;
    @Column(name = "paramid")
    private String paramId;
    @Column(name = "t01")
    private double t01;
    @Column(name = "t02")
    private double t02;
    @Column(name = "t03")
    private double t03;
    @Column(name = "t04")
    private double t04;
    @Column(name = "t05")
    private double t05;
    @Column(name = "t06")
    private double t06;
    @Column(name = "t07")
    private double t07;
    @Column(name = "t08")
    private double t08;
    @Column(name = "t09")
    private double t09;
    @Column(name = "t10")
    private double t10;
    @Column(name = "t11")
    private double t11;
    @Column(name = "t12")
    private double t12;
    @Column(name = "cn")
    private double cn;
    @Column(name = "user_cr_id")
    private String userCrId;
    @Column(name = "user_cr_dtime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime userCrDtime;
    @Column(name = "user_mdf_id")
    private String userMdfId;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "user_mdf_dtime")
    private LocalDateTime userMdfDtime;
}
