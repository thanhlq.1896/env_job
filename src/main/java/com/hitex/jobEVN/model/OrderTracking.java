package com.hitex.jobEVN.model;

import com.hitex.jobEVN.utils.Constant;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name =  Constant.ORDER_TRACKING,schema = Constant.SALE)
@Entity
@Data
public class OrderTracking {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "order_id")
    private Integer orderId;
    @Column(name = "status")
    private Integer status;
    @Column(name = "status_description")
    private String statusDescription;
    @Column(name = "status_name")
    private String statusName;
    @Column(name = "time")
    private LocalDateTime time;

}
