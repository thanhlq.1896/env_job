package com.hitex.jobEVN.model;

import com.hitex.jobEVN.utils.Constant;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name =  Constant.ORDER,schema = Constant.SALE)
@Entity
@Data
public class Order  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "created_at")
    private LocalDateTime createdAt;
    @Column(name = "status")
    private Integer status;
    @Column(name = "payment_type")
    private Integer paymentType;
    @Column(name = "email")
    private String email;
    @Column(name = "customer_name")
    private String customerName;
    @Column(name = "phone")
    private String phone;
    @Column(name = "code")
    private String code;
    @Column(name = "address")
    private String address;
    @Column(name = "m_delivery_fee")
    private String deliveryFee;
    @Column(name = "delivery_type")
    private Integer deliveryType;
    @Column(name = "m_total")
    private String total;
    @Column(name = "m_total_order")
    private String totalOrder;


}
