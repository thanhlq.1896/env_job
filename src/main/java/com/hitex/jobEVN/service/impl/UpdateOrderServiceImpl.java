package com.hitex.jobEVN.service.impl;

import com.hitex.jobEVN.model.*;
import com.hitex.jobEVN.repository.*;
import com.hitex.jobEVN.service.UpdateOrderService;
import com.hitex.jobEVN.utils.Constant;
import com.hitex.jobEVN.utils.EmailUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.time.LocalDateTime;
import java.util.List;

@Log4j2
@Service
public class UpdateOrderServiceImpl implements UpdateOrderService {
    @Autowired
    IsdnRepository isdnRepository;
    @Autowired
    RetailOrderRepository retailOrderRepository;
    @Autowired
    SimRepository simRepository;
    @Autowired
    LogFreeingRessourceBeforeRepository logFreeingRessourceBeforeRepository;
    @Autowired
    OrderRepository orderRepository;

    @Autowired
    OrderTrackingRepository orderTrackingRepository;

    @Autowired
    OrderConfigRepository orderConfigRepository;

    @Autowired
    EmailConfigRepository emailConfigRepository;

    @Autowired
    EmailHistoryRepository emailHistoryRepository;

    @Autowired
    PackageLanguageRepository packageLanguageRepository;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public int updateOrder(List<RetailOrder> retailOrderList, Order order, int success) {
        log.info("OrderID = " + order.getId() + " , Payment Type =  " + order.getPaymentType() + " ,Status = " + order.getStatus());
        if (order.getStatus() == Constant.WAIT_CONFIRM_OTP_MAIL) {
            success = cancelOrderOTP(retailOrderList, order, Constant.TIME_WAIT_BANK_TRANSFER, Constant.TIME_WAIT_BANK_TRANSFER_MINUTE, Constant.TIME_WAIT_BANK_TRANSFER_DATE, success);
        } else if (order.getStatus() == Constant.CONFIRMED_OTP_MAIL) {
            success = cancelOrderOTP(retailOrderList, order, Constant.TIME_WAIT_CONFIRM_INFO, Constant.TIME_WAIT_CONFIRM_INFO_MINUTE, Constant.TIME_WAIT_CONFIRM_INFO_DATE, success);
        }
        return success;
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public int updateOrderTransfer(List<RetailOrder> retailOrderList, Order order, int success, int amount) {
        log.info("Order Transfer with OrderID = " + order.getId() + " , Payment Type =  " + order.getPaymentType() + " ,Status = " + order
                .getStatus());
        for (RetailOrder retailOrder : retailOrderList) {
            boolean TRANSFER_DATE = false;
            String value = null;
            OrderTracking orderTracking = orderTrackingRepository.getTime(order.getId());
            if (ObjectUtils.isEmpty(orderTracking)) {
                log.info("OrderId not found in table OrderTracking" + LocalDateTime.now().toLocalTime());
            }
            if (Constant.WAIT_TRANSFER == order.getStatus()) {
                List<OrderConfig> orderConfig = orderConfigRepository.getCode(Constant.TIME_WAIT_BANK_TRANSFER);
                if (ObjectUtils.isEmpty(orderConfig)) {
                    log.info("TIME_WAIT_BANK_TRANSFER config does not exist in table order_config");
                }

                for (int i = 0; i < orderConfig.size(); i++) {
                    if (Constant.TIME_WAIT_BANK_TRANSFER_MINUTE.equals(orderConfig.get(i)
                                                                                  .getCode()) && !StringUtils.isEmpty(orderConfig
                            .get(i)
                            .getValue())) {
                        value = orderConfig.get(i).getValue();
                    } else if (Constant.TIME_WAIT_BANK_TRANSFER_DATE.equals(orderConfig.get(i)
                                                                                       .getCode()) && !StringUtils.isEmpty(orderConfig
                            .get(i)
                            .getValue())) {
                        TRANSFER_DATE = true;
                        value = orderConfig.get(i).getValue();
                    }
                }
                LocalDateTime formatDateTime = convertDate(TRANSFER_DATE, value, orderTracking);

                if (LocalDateTime.now().isAfter(formatDateTime)) {
                    LogFreeingResourceBefore logFreeingResourceBefore = new LogFreeingResourceBefore();
                    logFreeingResourceBefore.setOrderId(order.getId());
                    logFreeingResourceBefore.setRetailId(retailOrder.getId());
                    logFreeingResourceBefore.setStatusOrder(order.getStatus());
                    logFreeingResourceBefore.setStatusRetail(retailOrder.getStatus());
                    logFreeingResourceBefore.setPaymentTypeBefore(order.getPaymentType());
                    if (ObjectUtils.isEmpty(retailOrder)) {
                        logFreeingResourceBefore.setDescription("Ma Don Hang " + order.getId() + " khong ton tai trong bang Retail Order");
                    }
                    log.info("RetailID = " + retailOrder.getId()
                            + " , ISDN =  " + retailOrder.getIsdn()
                            + " ,Status = " + retailOrder.getStatus());
                    logFreeingResourceBefore.setProcessDate(LocalDateTime.now());

                    EmailConfig emailConfig = emailConfigRepository.getContentEmail(Constant.CANCEL_ORDER);
                    if (ObjectUtils.isEmpty(emailConfig)) {
                        log.info("Email content not config");
                    }
                    String content = emailConfig.getContent();
                    String LIST_ORDER = Constant.LIST_ORDER;
                    String PACK = Constant.PACK;
                    content = org.springframework.util.StringUtils.replace(content, "__name__", order.getCustomerName());
                    content = org.springframework.util.StringUtils.replace(content, "__address__", order.getAddress());
                    content = org.springframework.util.StringUtils.replace(content, "__phone__", order.getPhone());
                    content = org.springframework.util.StringUtils.replace(content, "__order_code__", order.getCode());
                    if (ObjectUtils.isEmpty(retailOrder.getPackageId())) {
                        content = org.springframework.util.StringUtils.replace(content, "__list_order__", Constant.LIST_ORDER);
                    } else {
                        if (ObjectUtils.isEmpty(retailOrder.getSimSerial())) {
                            retailOrder.setSimSerial("");
                        }
                        PackageLanguage packageLanguage = packageLanguageRepository.getPackInfo(retailOrder.getPackageId(), retailOrder
                                .getLanguage());
                        PACK = org.springframework.util.StringUtils.replace(PACK, "__package_name__", packageLanguage.getTitle());
                        PACK = org.springframework.util.StringUtils.replace(PACK, "__package_price__", String.valueOf(retailOrder
                                .getPackagePrice()));
                        PACK = org.springframework.util.StringUtils.replace(PACK, "__expire_date__", "60");

                        LIST_ORDER = org.springframework.util.StringUtils.replace(LIST_ORDER, "__isdn__", retailOrder.getIsdn());
                        LIST_ORDER = org.springframework.util.StringUtils.replace(LIST_ORDER, "__serial__", retailOrder.getSimSerial());
                        LIST_ORDER = org.springframework.util.StringUtils.replace(LIST_ORDER, "__isdn_price__", String.valueOf(retailOrder
                                .getIsdnPrice()));
                        LIST_ORDER = org.springframework.util.StringUtils.replace(LIST_ORDER, "__retail_code__", String.valueOf(retailOrder
                                .getCode()));
                        LIST_ORDER = org.springframework.util.StringUtils.replace(LIST_ORDER, "__qr_code__", String.valueOf(retailOrder
                                .getQrCode()));
                        LIST_ORDER = org.springframework.util.StringUtils.replace(LIST_ORDER, "_PACKAGE_", PACK);
                        content = org.springframework.util.StringUtils.replace(content, "__list_order__", LIST_ORDER);
                    }
                    content = org.springframework.util.StringUtils.replace(content, "__amount__", String.valueOf(amount));
                    if (Constant.DELIVERY_TYPE_SHIP_NORMAL == order.getDeliveryType()) {
                        content = org.springframework.util.StringUtils.replace(content, "__delivery_type__", Constant.DELIVERY_TYPE_NORMAL);
                    } else {
                        content = org.springframework.util.StringUtils.replace(content, "__delivery_type__", Constant.DELIVERY_TYPE__FAST);
                    }

                    content = org.springframework.util.StringUtils.replace(content, "__total_order__", order.getTotalOrder());
                    content = org.springframework.util.StringUtils.replace(content, "__delivery_fee__", order.getDeliveryFee());
                    content = org.springframework.util.StringUtils.replace(content, "__total__", order.getTotal());

                    boolean rs = EmailUtil.send(emailConfig, order.getEmail(), content);
                    if (!rs) {
                        log.info("SEND EMAIL FAILED");
                    } else {
                        log.info("SEND EMAIL SUCCESS");
                    }
                    ;
                    try {
                        EmailHistory his = new EmailHistory();
                        his.setSender(emailConfig.getEmail());
                        his.setAction(emailConfig.getAction());
                        his.setSubject(emailConfig.getSubject());
                        his.setContent(content);
                        his.setRecipient(order.getEmail());
                        his.setCreateDate(LocalDateTime.now());
                        EmailHistory save = emailHistoryRepository.save(his);
                        log.info("Email history : " + save);
                    } catch (Exception e) {
                        log.error(e);
                    }
                    //update Status Order
                    orderRepository.updateOrder(Constant.WAIT_CANCEL, order.getId());
                    success++;
                    logFreeingResourceBefore.setStatusRetailAfter(retailOrder.getStatus());
                    logFreeingResourceBefore.setStatusOrderAfter(Constant.WAIT_CANCEL);
                    logFreeingResourceBefore.setSoldAfter(Constant.SOLD);
                    logFreeingResourceBefore.setPaymentTypeAfter(order.getPaymentType());
                    //Lưu log
                    logFreeingRessourceBeforeRepository.save(logFreeingResourceBefore);
                }
            }

            if (Constant.WAIT_CANCEL == order.getStatus()) {
                {
                    List<OrderConfig> orderConfig = orderConfigRepository.getCode(Constant.TIME_WAIT_CANCEL_BANK_TRANSFER);
                    if (ObjectUtils.isEmpty(orderConfig)) {
                        log.info("TIME_WAIT_BANK_TRANSFER config does not exist in table order_config");
                    }
                    for (int i = 0; i < orderConfig.size(); i++) {
                        if (Constant.TIME_WAIT_CANCEL_BANK_TRANSFER_MINUTE.equals(orderConfig.get(i)
                                                                                             .getCode()) && !StringUtils
                                .isEmpty(orderConfig.get(i).getValue())) {
                            value = orderConfig.get(i).getValue();
                        } else if (Constant.TIME_WAIT_CANCEL_BANK_TRANSFER_DATE.equals(orderConfig.get(i)
                                                                                                  .getCode()) && !StringUtils
                                .isEmpty(orderConfig.get(i).getValue())) {
                            TRANSFER_DATE = true;
                            value = orderConfig.get(i).getValue();
                        }
                    }
                    LocalDateTime formatDateTime = convertDate(TRANSFER_DATE, value, orderTracking);
                    if (LocalDateTime.now().isAfter(formatDateTime)) {
                        success++;
                        LogFreeingResourceBefore logFreeingResourceBefore = new LogFreeingResourceBefore();
                        logFreeingResourceBefore.setOrderId(order.getId());
                        logFreeingResourceBefore.setRetailId(retailOrder.getId());
                        logFreeingResourceBefore.setStatusOrder(order.getStatus());
                        logFreeingResourceBefore.setStatusRetail(retailOrder.getStatus());
                        logFreeingResourceBefore.setPaymentTypeBefore(order.getPaymentType());
                        if (ObjectUtils.isEmpty(retailOrder)) {
                            logFreeingResourceBefore.setDescription("Ma Don Hang " + order.getId() + " khong ton tai trong bang Retail Order");
                        }
                        log.info("RetailID = " + retailOrder.getId()
                                + " , ISDN =  " + retailOrder.getIsdn()
                                + " ,Status = " + retailOrder.getStatus());
                        logFreeingResourceBefore.setProcessDate(LocalDateTime.now());
                        //Huy isdn
                        if (!StringUtils.isEmpty(retailOrder.getIsdn())) {
                            Isdn isdn = isdnRepository.findIsdn(retailOrder.getIsdn());
                            List<RetailOrder> listRetail = retailOrderRepository.findIsdnRetail(retailOrder.getIsdn());
                            log.info("ISDN =  " + isdn.getIsdn()
                                    + " ,sold = " + isdn.getSold());
                            if (CollectionUtils.isEmpty(listRetail)) {
                                //update sold Isdn
                                isdnRepository.updateIsdn(Constant.NOT_SOLD, retailOrder.getIsdn());
                            }
                            logFreeingResourceBefore.setIsdn(retailOrder.getIsdn());
                            logFreeingResourceBefore.setSold(isdn.getSold());
                        }

                        //Huy sim
                        if (!StringUtils.isEmpty(retailOrder.getSimSerial())) {
                            Sim sim = simRepository.findSim(retailOrder.getSimSerial());
                            log.info("ImSerial =  " + sim.getImSerial()
                                    + " ,sold = " + sim.getSold());
                            List<RetailOrder> listRetail = retailOrderRepository.findimSerialRetail(retailOrder.getSimSerial());
                            if (listRetail.size() == 1) {
                                //update sold sim serial
                                simRepository.updateSim(Constant.NOT_SOLD, retailOrder.getSimSerial());
                            }
                            logFreeingResourceBefore.setImSerial(retailOrder.getSimSerial());
                            logFreeingResourceBefore.setSold(sim.getSold());
                        }


                        if (retailOrder.getStatus() == 1 || retailOrder.getStatus() == 7) {
                            success++;
                            //update status Retail Order
                            retailOrderRepository.updateRetail(Constant.RO_REJECTED, retailOrder.getId());
                            //update Status Order
                            orderRepository.updateOrder(Constant.ORDER_CANCEL, order.getId());

                            logFreeingResourceBefore.setStatusRetailAfter(Constant.RO_REJECTED);
                            logFreeingResourceBefore.setStatusOrderAfter(Constant.ORDER_CANCEL);
                            logFreeingResourceBefore.setSoldAfter(Constant.NOT_SOLD);

                        } else {
                            logFreeingResourceBefore.setStatusRetailAfter(retailOrder.getStatus());
                            logFreeingResourceBefore.setStatusOrderAfter(order.getStatus());
                            logFreeingResourceBefore.setSoldAfter(Constant.SOLD);
                        }

                        logFreeingResourceBefore.setStatusRetailAfter(retailOrder.getStatus());
                        logFreeingResourceBefore.setStatusOrderAfter(Constant.ORDER_CANCEL);
                        logFreeingResourceBefore.setSoldAfter(Constant.NOT_SOLD);
                        logFreeingResourceBefore.setPaymentTypeAfter(order.getPaymentType());
                        //Lưu log
                        logFreeingRessourceBeforeRepository.save(logFreeingResourceBefore);
                    }
                }
            }
        }
        return success;
    }

    public LocalDateTime convertDate(Boolean TRANSFER_DATE, String value, OrderTracking orderTracking) {
        LocalDateTime formatDateTime = null;
        if (TRANSFER_DATE) {
            String[] valueItems = value.split("-");
            try {
                String ConvertDate = orderTracking.getTime()
                                                  .plusDays(Long.parseLong(valueItems[1]))
                                                  .format(Constant.formatDate);
                ConvertDate = ConvertDate + " " + valueItems[0] + ":00";
                formatDateTime = LocalDateTime.parse(ConvertDate, Constant.formatDateV2);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            formatDateTime = orderTracking.getTime().plusMinutes(Long.parseLong(value));
        }
        return formatDateTime;
    }

    public int cancelOrderOTP(List<RetailOrder> retailOrderList, Order order, String Time, String minute, String Date, int success) {
        for (RetailOrder retailOrder : retailOrderList) {
            boolean TRANSFER_DATE = false;
            String value = null;
            String[] valueItems;
            OrderTracking orderTracking = orderTrackingRepository.getTime(order.getId());
            if (ObjectUtils.isEmpty(orderTracking)) {
                log.info("OrderId not found in table OrderTracking" + LocalDateTime.now().toLocalTime());
            }
            List<OrderConfig> orderConfig = orderConfigRepository.getCode(Time);
            if (ObjectUtils.isEmpty(orderConfig)) {
                log.info("TIME_WAIT_BANK_TRANSFER config does not exist in table order_config");
            }

            for (int i = 0; i < orderConfig.size(); i++) {
                if (minute.equals(orderConfig.get(i).getCode()) && !StringUtils.isEmpty(orderConfig.get(i)
                                                                                                   .getValue())) {
                    value = orderConfig.get(i).getValue();
                } else if (Date.equals(orderConfig.get(i).getCode()) && !StringUtils.isEmpty(orderConfig.get(i)
                                                                                                        .getValue())) {
                    TRANSFER_DATE = true;
                    value = orderConfig.get(i).getValue();
                }
            }
            if (TRANSFER_DATE) {
                valueItems = value.split("-");
                try {
                    String ConvertDate = orderTracking.getTime()
                                                      .plusDays(Long.parseLong(valueItems[1]))
                                                      .format(Constant.formatDate);
                    ConvertDate = ConvertDate + " " + valueItems[0] + ":00";
                    LocalDateTime formatDateTime = LocalDateTime.parse(ConvertDate, Constant.formatDateV2);
                    log.info(ConvertDate);
                    log.info(formatDateTime);
                    log.info(LocalDateTime.now());
                    if (LocalDateTime.now().isAfter(formatDateTime)) {
                        LogFreeingResourceBefore logFreeingResourceBefore = new LogFreeingResourceBefore();
                        logFreeingResourceBefore.setOrderId(order.getId());
                        logFreeingResourceBefore.setRetailId(retailOrder.getId());
                        logFreeingResourceBefore.setStatusOrder(order.getStatus());
                        logFreeingResourceBefore.setStatusRetail(retailOrder.getStatus());
                        logFreeingResourceBefore.setPaymentTypeBefore(order.getPaymentType());
                        if (ObjectUtils.isEmpty(retailOrder)) {
                            logFreeingResourceBefore.setDescription("Ma Don Hang " + order.getId() + " khong ton tai trong bang Retail Order");
                        }
                        log.info("RetailID = " + retailOrder.getId()
                                + " , ISDN =  " + retailOrder.getIsdn()
                                + " ,Status = " + retailOrder.getStatus());
                        if (!StringUtils.isEmpty(retailOrder.getIsdn())) {
                            Isdn isdn = isdnRepository.findIsdn(retailOrder.getIsdn());
                            List<RetailOrder> listRetail = retailOrderRepository.findIsdnRetail(retailOrder.getIsdn());
                            log.info("ISDN =  " + isdn.getIsdn()
                                    + " ,sold = " + isdn.getSold());
                            if (CollectionUtils.isEmpty(listRetail)) {
                                //update sold Isdn
                                isdnRepository.updateIsdn(Constant.NOT_SOLD, retailOrder.getIsdn());
                            }
                            logFreeingResourceBefore.setIsdn(retailOrder.getIsdn());
                            logFreeingResourceBefore.setSold(isdn.getSold());
                        }

                        if (!StringUtils.isEmpty(retailOrder.getSimSerial())) {
                            Sim sim = simRepository.findSim(retailOrder.getSimSerial());
                            log.info("ImSerial =  " + sim.getImSerial()
                                    + " ,sold = " + sim.getSold());
                            List<RetailOrder> listRetail = retailOrderRepository.findimSerialRetail(retailOrder.getSimSerial());
                            if (listRetail.size() == 1) {
                                //update sold sim serial
                                simRepository.updateSim(Constant.NOT_SOLD, retailOrder.getSimSerial());
                            }
                            logFreeingResourceBefore.setImSerial(retailOrder.getSimSerial());
                            logFreeingResourceBefore.setSold(sim.getSold());
                        }


                        logFreeingResourceBefore.setProcessDate(LocalDateTime.now());


                        if (retailOrder.getStatus() == 1 || retailOrder.getStatus() == 7) {
                            success++;
                            //update status Retail Order
                            retailOrderRepository.updateRetail(Constant.RO_REJECTED, retailOrder.getId());
                            //update Status Order
                            orderRepository.updateOrder(Constant.ORDER_CANCEL, order.getId());

                            logFreeingResourceBefore.setStatusRetailAfter(Constant.RO_REJECTED);
                            logFreeingResourceBefore.setStatusOrderAfter(Constant.ORDER_CANCEL);
                            logFreeingResourceBefore.setSoldAfter(Constant.NOT_SOLD);

                        } else {
                            logFreeingResourceBefore.setStatusRetailAfter(retailOrder.getStatus());
                            logFreeingResourceBefore.setStatusOrderAfter(order.getStatus());
                            logFreeingResourceBefore.setSoldAfter(Constant.SOLD);
                        }
                        logFreeingResourceBefore.setPaymentTypeAfter(order.getPaymentType());
                        //Lưu log
                        logFreeingRessourceBeforeRepository.save(logFreeingResourceBefore);
//                            success+=success;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
        return success;
    }
}
