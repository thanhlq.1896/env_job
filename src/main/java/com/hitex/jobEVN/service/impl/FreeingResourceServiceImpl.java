package com.hitex.jobEVN.service.impl;

import com.hitex.jobEVN.model.Order;
import com.hitex.jobEVN.model.RetailOrder;
import com.hitex.jobEVN.repository.*;
import com.hitex.jobEVN.service.FreeingResourceService;
import com.hitex.jobEVN.service.UpdateOrderService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Log4j2
@Service
public class FreeingResourceServiceImpl implements FreeingResourceService {
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    RetailOrderRepository retailOrderRepository;
    @Autowired
    IsdnRepository isdnRepository;
    @Autowired
    SimRepository simRepository;
    @Autowired
    LogFreeingRessourceBeforeRepository logFreeingRessourceBeforeRepository;
    @Autowired(required = true)
    UpdateOrderService updateOrderService;


    @Scheduled(initialDelay = 1000, fixedDelayString = "${time.delay.notification}")
    @Override
    @Transactional
    public void FreeingResourceService() {
        try {
            int fail = 0;
            int success = 0;
            int wait = 0;
            // Lấy danh sách Order
            List<Order> orderAuhtenOTP = orderRepository.findOrderAuthenOTP();
            List<Order> orderTransfer = orderRepository.findOrderTransfer();
            log.info("Tong so orderAuthenOTP quet : " + orderAuhtenOTP.size());
            log.info("Tong so orderTransfer quet : " + orderTransfer.size());
            log.info("=============================Start Job Authen OTP==============================");
            for (Order order : orderAuhtenOTP) {
                try {
                    List<RetailOrder> retailOrders = retailOrderRepository.findRetailByOrderId(order.getId());
                    success = updateOrderService.updateOrder(retailOrders, order, success);
                } catch (Exception e) {
                    fail++;
                    log.error(e);
                }
            }
            log.info("=============================Start Job Check Transfer==============================");
            for (Order order : orderTransfer) {
                try {
                    List<RetailOrder> retailOrders = retailOrderRepository.findRetailByOrderId(order.getId());
                    wait = updateOrderService.updateOrderTransfer(retailOrders, order, wait, retailOrders.size());

                } catch (Exception e) {
                    fail++;
                    log.error(e);
                }
            }
            log.info("So order cho huy : " + wait);
            log.info("So order giai phong thanh cong : " + success);
            log.info("So order giai phong that bai : " + fail);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
