package com.hitex.jobEVN.service;

import com.hitex.jobEVN.model.Order;
import com.hitex.jobEVN.model.RetailOrder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public interface UpdateOrderService {

    @Transactional(rollbackFor = Exception.class)
    int updateOrder(List<RetailOrder> retailOrderList, Order order, int success);

    int updateOrderTransfer(List<RetailOrder> retailOrderList, Order order, int success, int amount);
}
